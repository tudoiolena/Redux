import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Paths } from "./enums/paths";
import { Bookings, Main, SignIn, SignUp, Trip } from "./pages";
import { ProtectedRoute } from "./utils/private-route";

const App = () => (
  <BrowserRouter>
    <Routes>
      <Route path={Paths.MAIN} element={<ProtectedRoute component={Main} />} />
      <Route path={Paths.LOGIN} element={<SignIn />} />
      <Route path={Paths.REGISTER} element={<SignUp />} />
      <Route
        path={`${Paths.TRIP}/:id`}
        element={<ProtectedRoute component={Trip} />}
      />
      <Route
        path={Paths.BOOKINGS}
        element={<ProtectedRoute component={Bookings} />}
      />
    </Routes>
  </BrowserRouter>
);

export default App;
