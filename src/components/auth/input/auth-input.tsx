import { FC, InputHTMLAttributes } from "react";
import {
  RegisterOptions,
  FieldErrors,
  FieldValues,
  UseFormRegister,
} from "react-hook-form";

interface IFormValues {
  name?: string;
  email: string;
  password: string;
}

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  testId: string;
  errors?: FieldErrors<FieldValues>;
  validationSchema?: RegisterOptions;
  register?: UseFormRegister<IFormValues>;
  name: keyof IFormValues;
}

export const AuthInput: FC<IProps> = ({
  label,
  testId,
  errors,
  name,
  validationSchema,
  register,
  ...inputProps
}) => {
  return (
    <>
      <label className="input">
        <span className="input__heading">{label}</span>
        <input
          data-test-id={testId}
          {...(register && register(name, validationSchema))}
          {...inputProps}
        />
      </label>
      <span>{errors && errors[name]?.message?.toString()}</span>
    </>
  );
};
