import { Link, useLocation } from "react-router-dom";
import { Paths } from "../../enums/paths";
import { useAppSelector } from "../../hooks/use-app-selector/use-app-selector";
import { useAppDispatch } from "../../hooks/use-app-dispatch/use-app-dispatch";
import { actions } from "../../store/actions";
import { useEffect } from "react";

export const Header = () => {
  const location = useLocation();
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.register.user);

  const isAuthPage =
    location.pathname === Paths.LOGIN || location.pathname === Paths.REGISTER;

  const handleSignOut = () => {
    dispatch(actions.signOut());
  };

  useEffect(() => {
    dispatch(actions.loadUserData());
  }, [dispatch]);

  return (
    <header className="header">
      <div className="header__inner">
        <Link
          data-test-id="header-logo"
          to={Paths.MAIN}
          className="header__logo"
        >
          Travel App
        </Link>
        {!isAuthPage && (
          <nav data-test-id="header-nav" className="header__nav">
            <ul className="nav-header__list">
              <li className="nav-header__item" title="Bookings">
                <Link
                  data-test-id="header-bookings-link"
                  to={Paths.BOOKINGS}
                  className="nav-header__inner"
                >
                  <span className="visually-hidden">Bookings</span>
                  <img src="/assets/images/briefcase.svg" alt="bookings" />
                </Link>
              </li>
              <li className="nav-header__item" title="Profile">
                <div
                  data-test-id="header-profile-nav"
                  className="nav-header__inner profile-nav"
                  tabIndex={0}
                >
                  <span className="visually-hidden">Profile</span>
                  <img src="/assets/images/user.svg" alt="profile" />
                  <ul
                    data-test-id="header-profile-nav-list"
                    className="profile-nav__list"
                  >
                    <li
                      data-test-id="header-profile-nav-username"
                      className="profile-nav__item"
                    >
                      {user?.fullName}
                    </li>
                    <li className="profile-nav__item">
                      <Link
                        data-test-id="header-profile-nav-sign-out"
                        to={Paths.LOGIN}
                        className="profile-nav__sign-out button"
                        onClick={handleSignOut}
                      >
                        Sign Out
                      </Link>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </nav>
        )}
      </div>
    </header>
  );
};
