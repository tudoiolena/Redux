import { ChangeEvent, FC } from "react";

interface IProprs {
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const SearchInput: FC<IProprs> = ({ onChange }) => (
  <label className="trips-filter__search input">
    <span className="visually-hidden">Search by name</span>
    <input
      data-test-id="filter-search"
      name="search"
      type="search"
      placeholder="search by title"
      onChange={onChange}
    />
  </label>
);
