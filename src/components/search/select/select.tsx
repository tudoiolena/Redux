import { ChangeEvent, FC, ReactNode } from "react";

interface IProps {
  label: string;
  dataTestId: string;
  name: string;
  options: { value: string; label: ReactNode }[];
  onChange: (event: ChangeEvent<HTMLSelectElement>) => void;
}

export const SearchSelect: FC<IProps> = ({
  label,
  options,
  dataTestId,
  name,
  onChange,
}) => {
  return (
    <label className="select">
      <span className="visually-hidden">{label}</span>
      <select data-test-id={dataTestId} name={name} onChange={onChange}>
        <option value="">{label}</option>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </label>
  );
};
