export enum Endpoints {
  AUTHENTICATED_USER = "auth/authenticated-user",
  LOGIN = "auth/sign-in",
  REGISTER = "auth/sign-up",
  TRIPS = "trips",
  BOOKINGS = "bookings",
}
