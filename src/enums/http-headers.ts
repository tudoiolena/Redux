export enum HttpHeaders {
  Accept = "Accept",
  Authorization = "Authorization",
  ContentType = "Content-Type",
}
