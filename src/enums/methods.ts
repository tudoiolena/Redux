export enum Methods {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  PATCH = "PATCH",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
}
