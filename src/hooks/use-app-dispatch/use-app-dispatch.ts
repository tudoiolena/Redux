import { useDispatch } from "react-redux";
import { AppDispatch } from "../../types/app-dispatch";

const useAppDispatch: () => AppDispatch = () => useDispatch<AppDispatch>();

export { useAppDispatch };
