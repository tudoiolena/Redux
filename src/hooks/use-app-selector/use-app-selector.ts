import { TypedUseSelectorHook, useSelector } from "react-redux";
import { RootState } from "../../types/root-state";

const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export { useAppSelector };
