import { useEffect } from "react";
import { Footer, Header, BookingItem } from "../../components";
import { useAppDispatch } from "../../hooks/use-app-dispatch/use-app-dispatch";
import { actions } from "../../store/actions";
import { useAppSelector } from "../../hooks/use-app-selector/use-app-selector";
import Loader from "../../utils/loader";
import {
  showErrorNotification,
  showSuccessNotification,
} from "../../utils/notifications";

export const Bookings = () => {
  const dispatch = useAppDispatch();
  const bookings = useAppSelector((state) => state.bookingsData.bookings);
  const isLoading = useAppSelector((state) => state.tripsData.isLoading);

  const handleCancelBooking = async (id: string) => {
    try {
      await dispatch(actions.deleteBooking({ id }));
      showSuccessNotification("Booking is canceled successfully!");
    } catch {
      showErrorNotification("Booking cancelation failed.");
    }
    await dispatch(actions.getUsersBookings());
  };

  useEffect(() => {
    dispatch(actions.getUsersBookings());
  }, [dispatch]);

  return (
    <>
      {isLoading && <Loader />}
      <Header />
      <main className="bookings-page">
        <h1 className="visually-hidden">Travel App</h1>
        <ul className="bookings__list">
          {bookings.map((booking) => (
            <BookingItem
              key={booking.id}
              id={booking.id}
              title={booking.trip.title}
              guests={booking.guests}
              date={booking.date}
              totalPrice={booking.totalPrice}
              onCancel={handleCancelBooking}
            />
          ))}
        </ul>
      </main>
      <Footer />
    </>
  );
};
