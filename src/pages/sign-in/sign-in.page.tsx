import { Paths } from "../../enums/paths";
import { Link, useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { AuthButton, AuthInput, Footer, Header } from "../../components";
import { useAppDispatch } from "../../hooks/use-app-dispatch/use-app-dispatch";
import { actions } from "../../store/actions";

interface ISignInFormValues {
  email: string;
  password: string;
}

export const SignIn = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ISignInFormValues>({
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const onSubmit: SubmitHandler<ISignInFormValues> = async (data) => {
    const response = await dispatch(
      actions.login({ email: data.email, password: data.password })
    );

    const responseData = response.payload;
    if (!!responseData.token) {
      localStorage.setItem("authToken", responseData.token);
      navigate(Paths.MAIN);
    }
  };

  return (
    <>
      <Header />
      <main className="sign-in-page">
        <h1 className="visually-hidden">Travel App</h1>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="sign-in-form"
          autoComplete="off"
        >
          <h2 className="sign-in-form__title">Sign In</h2>
          <AuthInput
            label="Email"
            testId="auth-email"
            name="email"
            type="email"
            register={register}
            errors={errors}
            validationSchema={{
              required: "Email is required",
            }}
          />
          <AuthInput
            label="Password"
            testId="auth-password"
            name="password"
            type="password"
            autoComplete="new-password"
            register={register}
            errors={errors}
            validationSchema={{
              required: "Password is required",
              minLength: {
                value: 3,
                message: "Password must be at least 3 charachters long",
              },
              maxLength: {
                value: 20,
                message: "Password must be at most 20 characters",
              },
            }}
          />
          <AuthButton label="Sign In" />
          <span>
            Don't have an account?
            <Link
              data-test-id="auth-sign-up-link"
              to={Paths.REGISTER}
              className="sign-in-form__link"
            >
              Sign Up
            </Link>
          </span>
        </form>
      </main>
      <Footer />
    </>
  );
};
