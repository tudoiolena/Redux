import {
  CSSProperties,
  ChangeEvent,
  FC,
  FormEvent,
  useEffect,
  useState,
} from "react";
import { useAppDispatch } from "../../../../hooks/use-app-dispatch/use-app-dispatch";
import { actions } from "../../../../store/actions";
import { useAppSelector } from "../../../../hooks/use-app-selector/use-app-selector";
import {
  showErrorNotification,
  showSuccessNotification,
} from "../../../../utils/notifications";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  modalContent: {
    id: string;
    title: string;
    duration: number;
    price: number;
    level: string;
  };
}

export const TripModal: FC<IProps> = ({ isOpen, onClose, modalContent }) => {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.register.user);
  const [date, setDate] = useState("");
  const [numGuests, setNumGuests] = useState(1);
  const [totalPrice, setTotalPrice] = useState(modalContent.price);

  const handleDateChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newDate = event.target.value;
    setDate(newDate);
  };

  const handleNumGuestsChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newNumGuests = parseInt(event.target.value, 10);
    setNumGuests(newNumGuests);

    const newTotalPrice = modalContent.price * newNumGuests;
    setTotalPrice(newTotalPrice);
  };

  const modalStyle: CSSProperties = {
    display: isOpen ? "flex" : "none",
  };

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();

    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    if (new Date(date) < tomorrow) {
      showErrorNotification("Date should be not earlier than tomorrow");
      return;
    }

    await dispatch(actions.loadUserData());

    try {
      await dispatch(
        actions.bookTrip({
          tripId: modalContent.id,
          userId: user?.id!,
          guests: numGuests,
          date: date,
        })
      );
      showSuccessNotification("Booking is successful!");
    } catch {
      showErrorNotification("Booking failed.");
    }

    onClose();
  };

  useEffect(() => {
    if (isOpen) {
      setDate("");
      setNumGuests(1);
      setTotalPrice(modalContent.price);
    }
  }, [isOpen, modalContent.price]);

  return (
    <div>
      <div className="modal" hidden={!isOpen} style={modalStyle}>
        <div data-test-id="book-trip-popup" className="book-trip-popup">
          <button
            data-test-id="book-trip-popup-close"
            className="book-trip-popup__close"
            onClick={onClose}
          >
            ×
          </button>
          <form
            className="book-trip-popup__form"
            autoComplete="off"
            onSubmit={handleSubmit}
          >
            <div className="trip-info">
              <h3
                data-test-id="book-trip-popup-title"
                className="trip-info__title"
              >
                {modalContent.title}
              </h3>
              <div className="trip-info__content">
                <span
                  data-test-id="book-trip-popup-duration"
                  className="trip-info__duration"
                >
                  <strong> {modalContent.duration}</strong> days
                </span>
                <span
                  data-test-id="book-trip-popup-level"
                  className="trip-info__level"
                >
                  {modalContent.level}
                </span>
              </div>
            </div>
            <label className="input">
              <span className="input__heading">Date</span>
              <input
                data-test-id="book-trip-popup-date"
                name="date"
                type="date"
                required
                onChange={handleDateChange}
              />
            </label>
            <label className="input">
              <span className="input__heading">Number of guests</span>
              <input
                data-test-id="book-trip-popup-guests"
                name="guests"
                type="number"
                min="1"
                max="10"
                value={numGuests}
                required
                onChange={handleNumGuestsChange}
              />
            </label>
            <span className="book-trip-popup__total">
              Total:
              <output
                data-test-id="book-trip-popup-total-value"
                className="book-trip-popup__total-value"
              >
                {totalPrice} $
              </output>
            </span>
            <button
              data-test-id="book-trip-popup-submit"
              className="button"
              type="submit"
            >
              Book a trip
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
