import { useEffect, useState } from "react";
import { Footer, Header, TripDetails } from "../../components";
import { TripModal } from "./components/trip-modal";
import { useParams } from "react-router-dom";
import { useAppDispatch } from "../../hooks/use-app-dispatch/use-app-dispatch";
import { useAppSelector } from "../../hooks/use-app-selector/use-app-selector";
import { actions } from "../../store/actions";
import Loader from "../../utils/loader";

export const Trip = () => {
  const { id } = useParams();
  const dispatch = useAppDispatch();
  const selectedTrip = useAppSelector((state) => state.tripsData.selectedTrip);
  const isLoading = useAppSelector((state) => state.tripsData.isLoading);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    if (id) {
      dispatch(actions.gettripById({ id }));
    }
  }, [dispatch, id]);

  return (
    <>
      {isLoading && <Loader />}
      {selectedTrip && (
        <>
          <Header />
          <main className="trip-page">
            <h1 className="visually-hidden">Travel App</h1>
            <TripDetails
              imageSrc={selectedTrip.image}
              title={selectedTrip.title}
              duration={selectedTrip.duration}
              level={selectedTrip.level}
              description={selectedTrip.description}
              price={selectedTrip.price}
              onBookTrip={openModal}
            />
          </main>
          <Footer />
          <TripModal
            isOpen={isModalOpen}
            onClose={closeModal}
            modalContent={selectedTrip!}
          ></TripModal>
        </>
      )}
    </>
  );
};
