import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiCaller } from "../utils/api-caller";
import { Endpoints } from "../enums/endpoints";
import { signOut } from "./slices/auth.slice";
import { Methods } from "../enums/methods";

export const loadUserData = createAsyncThunk(
  "auth/load-authenticated-user-data",
  async () => {
    const data = await apiCaller(Endpoints.AUTHENTICATED_USER, Methods.GET);
    return data;
  }
);

export const register = createAsyncThunk(
  "auth/sign-up",
  async ({
    fullName,
    email,
    password,
  }: {
    fullName: string;
    email: string;
    password: string;
  }) => {
    const data = await apiCaller(Endpoints.REGISTER, Methods.POST, {
      fullName,
      email,
      password,
    });
    return data;
  }
);

export const login = createAsyncThunk(
  "auth/sign-in",
  async ({ email, password }: { email: string; password: string }) => {
    const data = await apiCaller(Endpoints.LOGIN, Methods.POST, {
      email,
      password,
    });
    return data;
  }
);

export const getAllTrips = createAsyncThunk("trips/getAll", async () => {
  const data = await apiCaller(Endpoints.TRIPS, Methods.GET);
  return data;
});

export const gettripById = createAsyncThunk(
  "trips/getById",
  async ({ id }: { id: string }) => {
    const data = await apiCaller(`${Endpoints.TRIPS}/${id}`, Methods.GET);
    return data;
  }
);

export const bookTrip = createAsyncThunk(
  "bookings/book-a-trip",
  async ({
    tripId,
    userId,
    guests,
    date,
  }: {
    tripId: string;
    userId: string;
    guests: number;
    date: string;
  }) => {
    const data = await apiCaller(Endpoints.BOOKINGS, Methods.POST, {
      tripId,
      userId,
      guests,
      date,
    });
    return data;
  }
);

export const getUsersBookings = createAsyncThunk(
  "bookings/get-authenticated-user-bookings",
  async () => {
    const data = await apiCaller(Endpoints.BOOKINGS, Methods.GET);
    return data;
  }
);

export const deleteBooking = createAsyncThunk(
  "bookings/delete-booking",
  async ({ id }: { id: string }) => {
    const data = await apiCaller(`${Endpoints.BOOKINGS}/${id}`, Methods.DELETE);
    return data;
  }
);

const actions = {
  loadUserData,
  register,
  login,
  signOut,
  getAllTrips,
  gettripById,
  bookTrip,
  getUsersBookings,
  deleteBooking,
};

export { actions };
