import { combineReducers } from "@reduxjs/toolkit";
import authReducer from "./slices/auth.slice";
import tripsReducer from "./slices/trips.slice";
import bookingsReducer from "./slices/bookings.slice";

const rootReducer = combineReducers({
  register: authReducer,
  tripsData: tripsReducer,
  bookingsData: bookingsReducer,
});

export default rootReducer;
