import { createSlice } from "@reduxjs/toolkit";
import { loadUserData, login, register } from "../actions";

type User = {
  id: string;
  fullName: string;
  email: string;
  createdAt: string;
};

type State = {
  isLoading: boolean;
  user: User | null;
  error: string | null;
};

const initialState: State = {
  isLoading: false,
  user: null,
  error: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    signOut: (state) => {
      state.user = null;
      localStorage.removeItem("authToken");
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loadUserData.pending, (state) => {
      state.isLoading = true;
    });

    builder.addCase(loadUserData.fulfilled, (state, action) => {
      state.isLoading = false;
      state.user = action.payload;
      state.error = null;
    });
    builder.addCase(loadUserData.rejected, (state, action) => {
      state.isLoading = false;
      state.user = null;
      state.error = action.error ? action.error.message! : "An error occurred";
    });
    builder.addCase(register.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(register.fulfilled, (state, action) => {
      state.isLoading = false;
      state.user = action.payload;
      state.error = null;
    });
    builder.addCase(register.rejected, (state, action) => {
      state.isLoading = false;
      state.user = null;
      state.error = action.error ? action.error.message! : "An error occurred";
    });

    builder.addCase(login.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(login.fulfilled, (state, action) => {
      state.isLoading = false;
      state.user = action.payload;
      state.error = null;
    });
    builder.addCase(login.rejected, (state, action) => {
      state.isLoading = false;
      state.user = null;
      state.error = action.error ? action.error.message! : "An error occurred";
    });
  },
});

export const { signOut } = authSlice.actions;
export default authSlice.reducer;
