import { createSlice } from "@reduxjs/toolkit";
import { bookTrip, deleteBooking, getUsersBookings } from "../actions";

type Trip = {
  title: string;
  duration: string;
  price: number;
};

type Booking = {
  id: string;
  tripId: string;
  userId: string;
  guests: number;
  date: string;
  totalPrice: number;
  createdAt: string;
  trip: Trip;
};

type BookingsState = {
  bookings: Booking[];
  isLoading: boolean;
  error: string | null;
};

const initialState: BookingsState = {
  bookings: [],
  isLoading: false,
  error: null,
};

const bookingsSlice = createSlice({
  name: "bookings",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(bookTrip.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(bookTrip.fulfilled, (state, action) => {
      state.isLoading = false;
      state.error = null;
      state.bookings.push(action.payload);
    });
    builder.addCase(bookTrip.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error ? action.error.message! : "An error occurred";
    });

    builder.addCase(getUsersBookings.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(getUsersBookings.fulfilled, (state, action) => {
      state.isLoading = false;
      state.error = null;
      state.bookings = action.payload;
    });
    builder.addCase(getUsersBookings.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error ? action.error.message! : "An error occurred";
    });
    builder.addCase(deleteBooking.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(deleteBooking.fulfilled, (state, action) => {
      state.isLoading = false;
      state.error = null;
      state.bookings = state.bookings.filter(
        (booking) => booking.id !== action.payload
      );
    });
    builder.addCase(deleteBooking.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error ? action.error.message! : "An error occurred";
    });
  },
});

export default bookingsSlice.reducer;
