import { createSlice } from "@reduxjs/toolkit";
import { getAllTrips, gettripById } from "../actions";

type Trip = {
  id: string;
  title: string;
  description: string;
  level: string;
  duration: number;
  price: number;
  image: string;
  createdAt: string;
};

type State = {
  allTrips: Array<Trip>;
  selectedTrip: Trip | null;
  isLoading: boolean;
  error: string | null;
};

const initialState: State = {
  allTrips: [],
  selectedTrip: null,
  isLoading: false,
  error: null,
};

const tripsSlice = createSlice({
  name: "trips",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllTrips.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(getAllTrips.fulfilled, (state, action) => {
      state.isLoading = false;
      state.allTrips = action.payload;
      state.error = null;
    });
    builder.addCase(getAllTrips.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message || "An error occurred";
    });
    builder.addCase(gettripById.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(gettripById.fulfilled, (state, action) => {
      state.isLoading = false;
      state.selectedTrip = action.payload;
      state.error = null;
    });
    builder.addCase(gettripById.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message || "An error occurred";
    });
  },
});

export default tripsSlice.reducer;
