import { RootState } from "./root-state";
import { AppDispatch } from "./app-dispatch";

type AsyncThunkConfig = {
  state: RootState;
  dispatch: AppDispatch;
};

export type { AsyncThunkConfig };
