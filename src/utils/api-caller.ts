import { HttpHeaders } from "../enums/http-headers";
import { Paths } from "../enums/paths";

export const getToken = () => {
  return localStorage.getItem("authToken") ?? "";
};

export const apiCaller = async (url: string, method: string, body?: any) => {
  const baseUrl = "https://travel-app-api.up.railway.app/api/v1";
  const token = getToken();

  const headers: Record<string, string> = {
    [HttpHeaders.Accept]: "application/json",
    [HttpHeaders.ContentType]: "application/json",
  };

  if (
    token &&
    !baseUrl.includes(Paths.REGISTER) &&
    !baseUrl.includes(Paths.LOGIN)
  ) {
    headers[HttpHeaders.Authorization] = `Bearer ${token}`;
  }

  const response = await fetch(`${baseUrl}/${url}`, {
    method,
    headers,
    body: body && JSON.stringify(body),
  });
  if (!response.ok) {
    throw new Error(`HTTP error! Status: ${response.status}`);
  }

  const jsonResponse = await response.json();

  return jsonResponse;
};
