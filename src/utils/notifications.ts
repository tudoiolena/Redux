import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const showSuccessNotification = (message: string) => {
  toast.success(message, {
    className: "notification",
  });
};

export const showErrorNotification = (message: string) => {
  toast.error(message, {
    className: "notification",
  });
};
