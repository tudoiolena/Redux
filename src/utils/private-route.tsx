import { FC } from "react";
import { Navigate } from "react-router-dom";
import { Paths } from "../enums/paths";

interface IProps {
  component: FC;
}

export const ProtectedRoute: FC<IProps> = ({ component: Component }) => {
  const accessToken = localStorage.getItem("authToken");

  return accessToken ? <Component /> : <Navigate to={Paths.LOGIN} />;
};
